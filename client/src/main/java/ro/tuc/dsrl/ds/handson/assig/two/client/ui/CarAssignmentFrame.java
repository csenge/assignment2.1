package ro.tuc.dsrl.ds.handson.assig.two.client.ui;

import ro.tuc.dsrl.ds.handson.assig.two.client.communication.ServerConnection;
import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;
import ro.tuc.dsrl.ds.handson.assig.two.rpc.Naming;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by csenge on 10.11.2016.
 */
public class CarAssignmentFrame extends JFrame {

public CarAssignmentFrame(){
    init();
}

    private void init() {
        setTitle("Car Management");
        setSize(500, 500);
        setLayout(null);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JLabel addCarLabel = new JLabel();
        addCarLabel.setText("Please provide the necessary information about your car: ");
        addCarLabel.setBounds(0,0,500,20);
        add(addCarLabel);

        JLabel yearLabel = new JLabel();
        yearLabel.setText("Year: ");
        yearLabel.setBounds(5,30,100,20);
        add(yearLabel);

        JTextField yearTF = new JTextField();
        yearTF.setBounds(5,55,100,20);
        add(yearTF);

        JLabel engineLabel = new JLabel();
        engineLabel.setText("Engine size: ");
        engineLabel.setBounds(5,85,100,20);
        add(engineLabel);

        JTextField engineTF = new JTextField();
        engineTF.setBounds(5,110,100,20);
        add(engineTF);

        JLabel priceLabel = new JLabel();
        priceLabel.setText("Purchasing price: ");
        priceLabel.setBounds(5,130,200,20);
        add(priceLabel);

       JTextField priceTF = new JTextField();
        priceTF.setBounds(5,160,100,20);
        add(priceTF);

       JButton getTaxButton = new JButton();
        getTaxButton.setBounds(5, 220, 150,20);
        getTaxButton.setText("TAX");
        add(getTaxButton);

        JButton getPriceButton = new JButton();
        getPriceButton.setBounds(5, 260, 150, 20);
        getPriceButton.setText("SELLING PRICE");
        add(getPriceButton);

        JTextArea results = new JTextArea();
        results.setAutoscrolls(true);
        results.setEditable(false);
        results.setBounds(0, 300, 400, 150);
        add(results);

        getTaxButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                String yearString = yearTF.getText();
                String engineString = engineTF.getText();
                String priceString = priceTF.getText();

                int year = Integer.parseInt(yearString);
                int engine = Integer.parseInt(engineString);
                double price = Double.parseDouble(priceString);

                ITaxService taxService = null;

                try {
                    taxService = Naming.lookup(ITaxService.class,
                            ServerConnection.getInstance());
                    double tax = taxService.computeTax(new Car(year, engine, price));
                    results.append("Year: "+ yearString+ ", Engine: "+yearString+ ", Purchasing price: "+priceString +", Tax: "+tax+". \n");

                } catch (Exception e) {
                    System.out.println(e.toString());                }
            }
        });

        getPriceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //TODO
                String yearString = yearTF.getText();
                String engineString = engineTF.getText();
                String priceString = priceTF.getText();

                int year = Integer.parseInt(yearString);
                int engine = Integer.parseInt(engineString);
                double price = Double.parseDouble(priceString);

                IPriceService priceService= null;

                try {
                    priceService = Naming.lookup(IPriceService.class,
                            ServerConnection.getInstance());
                    double sellingPrice = priceService.computePrice(new Car(year, engine, price));
                    results.append("Year: "+ yearString+ ", Engine: "+yearString+ ", Purchasing price: "+priceString +", Selling price: "+sellingPrice+". \n");

                } catch (Exception e) {

                    System.out.println(e.getStackTrace());
                }
            }

        });


    }


}
